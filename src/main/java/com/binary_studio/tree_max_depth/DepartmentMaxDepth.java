package com.binary_studio.tree_max_depth;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {

		if(rootDepartment == null) return 0;
		return 1 + rootDepartment.subDepartments.stream()
				.map(DepartmentMaxDepth::calculateMaxDepth)
				.max(Integer::compareTo)
				.orElse(0);


	}

}
