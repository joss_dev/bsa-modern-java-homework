package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

    private final String name;
    private final PositiveInteger powergridRequirments;
    private final PositiveInteger capacitorConsumption;
    private final PositiveInteger optimalSpeed;
    private final PositiveInteger optimalSize;
    private final PositiveInteger baseDamage;

    public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
        this.name = name;
        this.powergridRequirments = powergridRequirments;
        this.capacitorConsumption = capacitorConsumption;
        this.optimalSpeed = optimalSpeed;
        this.optimalSize = optimalSize;
        this.baseDamage = baseDamage;
    }

    public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                                PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
                                                PositiveInteger baseDamage) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name should be not null and not empty");
        return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize, baseDamage);
    }

    @Override
    public PositiveInteger getPowerGridConsumption() {
        return powergridRequirments;
    }

    @Override
    public PositiveInteger getCapacitorConsumption() {
        return capacitorConsumption;
    }

    @Override
    public PositiveInteger attack(Attackable target) {
        double sizeReductionModifier = target.getSize().value() >= optimalSize.value() ? 1.0 : target.getSize().value().doubleValue() / optimalSize.value();
        double speedReductionModifier = target.getCurrentSpeed().value() <= optimalSpeed.value() ? 1.0 : optimalSpeed.value() / (2.0 * target.getCurrentSpeed().value());
        double damage = baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);
        return PositiveInteger.of((int) Math.ceil(damage));
    }

    @Override
    public String getName() {
        return name;
    }

}
