package com.binary_studio.fleet_commander.core.ship;

import java.math.BigInteger;
import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import javax.annotation.Nullable;

public final class CombatReadyShip implements CombatReadyVessel {

    private final String name;
    private PositiveInteger shieldHP;
    private final PositiveInteger maxShiedHP;
    private PositiveInteger hullHP;
    private final PositiveInteger maxHullHP;
    private PositiveInteger capacitorAmount;
    private final PositiveInteger capacitorMaxAmount;
    private final PositiveInteger capacitorRechargeRate;
    private final PositiveInteger speed;
    private final PositiveInteger size;
    private final AttackSubsystem attackSubsystem;
    private final DefenciveSubsystem defenciveSubsystem;

    public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                           PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
                           PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
        this.name = name;
        this.shieldHP = shieldHP;
        this.maxShiedHP = shieldHP;
        this.hullHP = hullHP;
        this.maxHullHP = hullHP;
        this.capacitorAmount = capacitorAmount;
        this.capacitorMaxAmount = capacitorAmount;
        this.capacitorRechargeRate = capacitorRechargeRate;
        this.speed = speed;
        this.size = size;
        this.attackSubsystem = attackSubsystem;
        this.defenciveSubsystem = defenciveSubsystem;
    }

    @Override
    public void endTurn() {
        capacitorAmount = PositiveInteger.of(Math.min(capacitorAmount.value() + capacitorRechargeRate.value(), capacitorMaxAmount.value()));
    }

    @Override
    public void startTurn() {
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public PositiveInteger getSize() {
        return size;
    }

    @Override
    public PositiveInteger getCurrentSpeed() {
        return speed;
    }

    @Override
    public Optional<AttackAction> attack(Attackable target) {
        if (capacitorAmount.value() < attackSubsystem.getCapacitorConsumption().value()) return Optional.empty();
        capacitorAmount = PositiveInteger.of(capacitorAmount.value() - attackSubsystem.getCapacitorConsumption().value());
        final AttackAction attackAction = new AttackAction(attackSubsystem.attack(target), this, target, attackSubsystem);
        return Optional.of(attackAction);
    }

    @Override
    public AttackResult applyAttack(AttackAction attack) {
        final int damage = defenciveSubsystem.reduceDamage(attack).damage.value();
        final int hullDamage = shieldHP.value() < damage ? damage - shieldHP.value() : 0;
        shieldHP = PositiveInteger.of(Math.max(shieldHP.value() - damage, 0));
        hullHP = PositiveInteger.of(Math.max(hullHP.value() - hullDamage, 0));
        if (hullHP.value() == 0) return new AttackResult.Destroyed();
        return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damage), this);
    }

    @Override
    public Optional<RegenerateAction> regenerate() {
        if (capacitorAmount.value() < defenciveSubsystem.getCapacitorConsumption().value()) return Optional.empty();
        capacitorAmount = PositiveInteger.of(capacitorAmount.value() - defenciveSubsystem.getCapacitorConsumption().value());
        final RegenerateAction regenerateAction = defenciveSubsystem.regenerate();
        int newHullHP = Math.min(hullHP.value() + regenerateAction.hullHPRegenerated.value(), maxHullHP.value());
        int newShieldHP = Math.min(shieldHP.value() + regenerateAction.shieldHPRegenerated.value(), maxShiedHP.value());
        final RegenerateAction cappedAction = new RegenerateAction(
                PositiveInteger.of(newShieldHP - shieldHP.value()),
                PositiveInteger.of(newHullHP - hullHP.value())
                );
        return Optional.of(cappedAction);
    }

}
