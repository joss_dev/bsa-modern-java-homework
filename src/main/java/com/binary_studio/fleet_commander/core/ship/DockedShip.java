package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;
	private final PositiveInteger shieldHP;
	private final PositiveInteger hullHP;
	private final PositiveInteger powergridOutput;
	private final PositiveInteger capacitorAmount;
	private final PositiveInteger capacitorRechargeRate;
	private final PositiveInteger speed;
	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem = null;
	private DefenciveSubsystem defenciveSubsystem = null;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
									   PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
									   PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed, size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			attackSubsystem = null;
			return;
		}
		int currentConsumption = defenciveSubsystem != null ? defenciveSubsystem.getPowerGridConsumption().value() : 0;
		int availableEnergy = powergridOutput.value() - subsystem.getPowerGridConsumption().value() - currentConsumption;
		if (availableEnergy < 0) {
			throw new InsufficientPowergridException(-availableEnergy);
		}
		attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			defenciveSubsystem = null;
			return;
		}
		int currentConsumption = attackSubsystem != null ? attackSubsystem.getPowerGridConsumption().value() : 0;
		int availableEnergy = powergridOutput.value() - subsystem.getPowerGridConsumption().value() - currentConsumption;
		if (availableEnergy < 0) {
			throw new InsufficientPowergridException(-availableEnergy);
		}
		defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (defenciveSubsystem == null && attackSubsystem == null) throw NotAllSubsystemsFitted.bothMissing();
		if (attackSubsystem == null) throw NotAllSubsystemsFitted.attackMissing();
		if (defenciveSubsystem == null) throw NotAllSubsystemsFitted.defenciveMissing();
		return new CombatReadyShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, speed, size, attackSubsystem, defenciveSubsystem);
	}

}
